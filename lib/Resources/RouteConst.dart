class RouteConst {
  static const String routeDefault = "/";
  static const routeHomePage = "/HomePage";
  static const routeVideosPage = "/VideosPage";
  static const routeQuestionsPage = "/QuestionsPage";
  static const routeCongratsPage = "/CongratsPage";
}
