import 'dart:io';

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomOrientationControls extends StatefulWidget {
  final FlickManager flickManager;
  final bool isForward;
  final String videoUrl;
  final int maxSeekTime;
  final Function addNotes;

  CustomOrientationControls(
      {Key key,
      @required this.videoUrl,
      this.flickManager,
      this.isForward = false,
      this.maxSeekTime = 0,
      this.addNotes})
      : super(key: key);

  @override
  CustomOrientationControlsState createState() =>
      CustomOrientationControlsState();
}

class CustomOrientationControlsState extends State<CustomOrientationControls> {
  String selectedVideoQuality;
  String selectedVideo;
  int seekTime = 0;
  String selectedVideoSpeed;

  @override
  void initState() {
    super.initState();
    selectedVideo = widget.videoUrl;
  }

  @override
  Widget build(BuildContext context) {
    FlickVideoManager flickVideoManager =
        Provider.of<FlickVideoManager>(context);
    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: FlickAutoHideChild(
            child: Container(
              color: Colors.black38,
            ),
          ),
        ),
        if (widget.flickManager.flickVideoManager.isBuffering)
          Center(
            child: Container(
              width: 35,
              height: 35,
              child: CircularProgressIndicator(
                strokeWidth: 3.0,
              ),
            ),
          ),
        if (!widget.flickManager.flickVideoManager.isBuffering)
          Positioned.fill(
            child: FlickShowControlsAction(
              child: FlickSeekVideoAction(
                duration: (widget.isForward) ? Duration(seconds: 15) : null,
                child: Center(
                  child: flickVideoManager.nextVideoAutoPlayTimer != null
                      ? FlickAutoPlayCircularProgress(
                          colors: FlickAutoPlayTimerProgressColors(
                            backgroundColor: Colors.white30,
                            color: Colors.red,
                          ),
                        )
                      : FlickAutoHideChild(
                          child: Stack(
                            children: [
                              Container(
                                alignment: Alignment.center,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    // if (widget.isForward)
                                    Padding(
                                      padding: const EdgeInsets.only(right: 0),
                                      child: GestureDetector(
                                        onTap: () {
                                          if (widget.isForward) {
                                            widget.flickManager
                                                .flickControlManager
                                                .seekBackward(
                                                    Duration(seconds: 15));
                                          } else {
                                            var curInsec = widget
                                                    .flickManager
                                                    .flickVideoManager
                                                    .videoPlayerController
                                                    .value
                                                    .position
                                                    .inSeconds +
                                                15;
                                            if (curInsec >= 0) {
                                              widget.flickManager
                                                  .flickControlManager
                                                  .seekBackward(
                                                      Duration(seconds: 15));
                                            }
                                          }
                                        },
                                        child: Icon(
                                          Icons.fast_rewind,
                                          color: Colors.white,
                                          size: 25,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: (widget
                                                  .flickManager
                                                  .flickVideoManager
                                                  .isVideoInitialized &&
                                              !widget
                                                  .flickManager
                                                  .flickVideoManager
                                                  .isBuffering)
                                          ? FlickPlayToggle(
                                              size: 40,
                                            )
                                          : Container(),
                                    ),
                                    // if (widget.isForward)
                                    Padding(
                                      padding: const EdgeInsets.only(left: 0.0),
                                      child: InkWell(
                                        onTap: () {
                                          if (widget.isForward) {
                                            widget.flickManager
                                                .flickControlManager
                                                .seekForward(
                                                    Duration(seconds: 15));
                                          } else {
                                            var curInsec = widget
                                                    .flickManager
                                                    .flickVideoManager
                                                    .videoPlayerController
                                                    .value
                                                    .position
                                                    .inSeconds +
                                                15;
                                            if (curInsec <=
                                                widget.maxSeekTime) {
                                              widget.flickManager
                                                  .flickControlManager
                                                  .seekForward(
                                                      Duration(seconds: 15));
                                            }
                                          }
                                        },
                                        child: Icon(
                                          Icons.fast_forward,
                                          color: Colors.white,
                                          size: 25,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                ),
              ),
            ),
          ),
        Positioned.fill(
          child: FlickAutoHideChild(
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          FlickCurrentPosition(
                            fontSize: 13.0,
                          ),
                          Text(
                            ' / ',
                            style:
                                TextStyle(color: Colors.white, fontSize: 13.0),
                          ),
                          FlickTotalDuration(
                            fontSize: 13.0,
                          ),
                        ],
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.border_color,
                                color: Colors.white,
                                size: 20,
                              ),
                              onPressed: widget.addNotes),
                          FlickFullScreenToggle(
                            size: 25.0,
                            padding: EdgeInsets.only(right: 10.0),
                          )
                        ],
                      )
                    ],
                  ),
                  Container(
                    height: 20,
                    padding: const EdgeInsets.all(2.0),
                    child: FlickVideoProgressBar(
                      flickProgressBarSettings: FlickProgressBarSettings(
                        height: 4,
                        handleRadius: 7,
                        curveRadius: 50,
                        backgroundColor: Colors.white24,
                        bufferedColor: Colors.white70,
                        playedColor: Color(0xff4378FF),
                        handleColor: Color(0xff4378FF),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  updateSeekTime() {
    if (widget.flickManager.flickVideoManager.isVideoInitialized) {
      widget.flickManager.flickControlManager
          .seekTo(Duration(seconds: seekTime));
      // if (Platform.isIOS) Future.delayed(Duration(seconds: 3)).then((value) {});
      widget.flickManager.flickVideoManager.removeListener(updateSeekTime);
    }
  }
}
