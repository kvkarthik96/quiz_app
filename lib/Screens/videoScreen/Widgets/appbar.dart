import 'package:flutter/material.dart';
import 'package:quiz_app/Resources/RouteConst.dart';

Widget appbar(context, title, flickManager, myNotes) {
  return AppBar(
    brightness: Brightness.light,
    // backgroundColor: Color(0xffF3F5F9),
    elevation: 0.0,
    title: Text(
      title,
      style: TextStyle(color: Colors.white),
    ),
    centerTitle: true,
    leading: InkWell(
      onTap: () {
        Navigator.canPop(context);
        if (flickManager.flickControlManager.isFullscreen) {
          flickManager.flickControlManager.exitFullscreen();
        } else {
          Navigator.of(context).pushReplacementNamed(
            RouteConst.routeHomePage,
          );
        }
      },
      child: Icon(Icons.arrow_back_ios, size: 25, color: Colors.white),
    ),
    actions: [
      IconButton(
          icon: Icon(
            Icons.border_color,
            color: Colors.white,
            size: 20,
          ),
          onPressed: myNotes)
    ],
  );
}
