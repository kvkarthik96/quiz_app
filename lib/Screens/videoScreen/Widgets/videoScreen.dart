import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quiz_app/Utils/VideoPlayerControls.dart';

Widget videoScreen(context, flickManager, url, addNotes) {
  return Container(
    width: MediaQuery.of(context).size.width,
    height: MediaQuery.of(context).size.height * .30,
    child: FlickVideoPlayer(
      flickManager: flickManager,
      wakelockEnabled: true,
      wakelockEnabledFullscreen: true,
      preferredDeviceOrientationFullscreen: [
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ],
      flickVideoWithControls: FlickVideoWithControls(
        videoFit: BoxFit.fill,
        controls: CustomOrientationControls(
          flickManager: flickManager,
          isForward: true,
          videoUrl: url,
          addNotes: addNotes,
        ),
      ),
      flickVideoWithControlsFullscreen: FlickVideoWithControls(
        videoFit: BoxFit.fitHeight,
        controls: CustomOrientationControls(
          flickManager: flickManager,
          isForward: true,
          videoUrl: url,
          addNotes: addNotes,
        ),
      ),
    ),
  );
}
