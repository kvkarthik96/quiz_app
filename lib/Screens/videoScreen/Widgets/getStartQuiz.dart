import 'package:flutter/material.dart';
import 'package:quiz_app/Resources/RouteConst.dart';

Widget getStartQuizWidget(context, id, title) {
  return Container(
    child: ButtonBar(
      buttonHeight: 50,
      buttonMinWidth: 150,
      alignment: MainAxisAlignment.center,
      children: <Widget>[
        FlatButton.icon(
          onPressed: () {
            Map<String, dynamic> data = {
              "id": id,
              "title": title,
            };

            Navigator.of(context).pushReplacementNamed(
                RouteConst.routeQuestionsPage,
                arguments: {"data": data});
          },
          label: Text('Start Quiz!'),
          icon: Icon(Icons.poll),
          color: Colors.green,
        )
      ],
    ),
  );
}
