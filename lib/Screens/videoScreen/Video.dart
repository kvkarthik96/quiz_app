import 'dart:ui';

import 'package:connectivity/connectivity.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:quiz_app/Resources/RouteConst.dart';
import 'package:quiz_app/Utils/HelperUtil.dart';
import 'package:quiz_app/Utils/LoadingUtil.dart';
import 'package:video_player/video_player.dart';

import 'Widgets/appbar.dart';
import 'Widgets/getStartQuiz.dart';
import 'Widgets/videoScreen.dart';

class VideoScreen extends StatefulWidget {
  final int id;
  final String title;
  final String videoUrl;

  const VideoScreen({Key key, this.id, this.title, this.videoUrl})
      : super(key: key);

  @override
  _VideoScreenState createState() => _VideoScreenState();
}

class _VideoScreenState extends State<VideoScreen> with WidgetsBindingObserver {
  FlickManager flickManager;

  bool isNetworkConnected = true;
  var _connectivitySubscription;
  String videoUrl;

  bool isLoading = false;
  int maxPlayedSeconds = 0;
  bool _isPlaying = false;

  var notesTime;
  int noteTimeInSec;
  bool showAddNote = false;
  bool isVideoComplets = false;
  bool isDisplayNote = true;
  String displayNoteText = "";

  List<String> notesTimeArray = [];
  List<String> notesArray = [];

  final TextEditingController notesController = new TextEditingController();
  final FocusNode notesFocusNode = FocusNode();
  String notes = "";
  final key = GlobalKey<FormFieldState>();

  /* On change in network wifi/mobile data */
  void onNetworkChange() {
    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        if (result == ConnectivityResult.none) {
          isNetworkConnected = false;
          if (flickManager != null) {
            if (flickManager.flickVideoManager.isVideoInitialized) {
              if (flickManager.flickVideoManager.isPlaying) {
                flickManager.flickControlManager.pause();
              }
            }
          }
        } else {
          isNetworkConnected = true;
          setUpVideoPlayer(videoUrl);
        }
      });
    });
  }

  /* Video Managers call */
  setUpVideoPlayer(String url) async {
    if (flickManager != null) {
      flickManager.handleChangeVideo(
        VideoPlayerController.network(url),
      );
    } else {
      flickManager = FlickManager(
        videoPlayerController: VideoPlayerController.network(url),
        autoPlay: true,
        onVideoEnd: onVideoCompleted,
      );
    }
    flickManager.flickVideoManager.addListener(videoListeners);
  }

  videoListeners() {
    if (_isPlaying != flickManager.flickVideoManager.isPlaying) {
      _isPlaying = flickManager.flickVideoManager.isPlaying;
    }
  }

  onVideoCompleted() async {
    flickManager.flickControlManager.pause();
    flickManager.flickVideoManager.removeListener(videoListeners);
    if (flickManager.flickControlManager.isFullscreen) {
      flickManager.flickControlManager.exitFullscreen();
    }
    if (flickManager != null &&
        flickManager.flickVideoManager.videoPlayerController.value.position
                .inSeconds >
            30) {
      setState(() {
        isVideoComplets = true;
      });
    }
  }

  updateProgressLoader(bool flag) {
    if (flag != isLoading) {
      setState(() {
        isLoading = flag;
      });
    }
  }

  void showAddNoteWidget() {
    setState(() {
      showAddNote = !showAddNote;
    });
  }

  void addNotes() {
    if (flickManager != null) {
      if (flickManager.flickVideoManager.isVideoInitialized) {
        if (flickManager.flickVideoManager.isPlaying) {
          flickManager.flickControlManager.pause();
        }
      }
    }
    var position =
        flickManager.flickVideoManager.videoPlayerController.value.position;
    String positionInSeconds = position != null
        ? (position - Duration(minutes: position.inMinutes))
            .inSeconds
            .toString()
            .padLeft(2, '0')
        : null;

    noteTimeInSec = position.inSeconds;
    notesTime =
        position != null ? '${position.inMinutes}:$positionInSeconds' : '0:00';
    showAddNoteWidget();
  }

  @override
  void initState() {
    super.initState();

    videoUrl = widget.videoUrl;

    onNetworkChange();
    setUpPlayer();
    HelperUtil.checkInternetConnection().then((connected) {
      setState(() {
        if (connected) {
          isNetworkConnected = connected;
          setUpVideoPlayer(videoUrl);
        } else {
          isNetworkConnected = connected;
        }
      });
    });
  }

  void setUpPlayer() {
    setState(() {
      setUpVideoPlayer(videoUrl);
    });
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    flickManager.flickVideoManager.removeListener(videoListeners);
    flickManager.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    flickManager.flickVideoManager.addListener(videoListeners);
    return WillPopScope(
      onWillPop: () {
        if (flickManager.flickControlManager.isFullscreen) {
          flickManager.flickControlManager.exitFullscreen();
        } else {
          Navigator.of(context).pushReplacementNamed(
            RouteConst.routeHomePage,
          );
        }
      },
      child: Scaffold(
        backgroundColor: Color(0xffF3F5F9),
        appBar: appbar(context, widget.title, flickManager, showMyNotes),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 15.0),
                child: (flickManager != null)
                    ? videoScreen(context, flickManager, videoUrl, addNotes)
                    : Container(
                        color: Colors.black,
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * .30,
                        child: LoadingUtil.pacman(context),
                      ),
              ),
              Expanded(
                child: Container(
                  height: double.infinity,
                ),
              ),
              if (showAddNote)
                Container(
                  color: Colors.white,
                  padding: new EdgeInsets.all(10.0),
                  child: Column(
                    children: [
                      new TextField(
                        key: key,
                        controller: notesController,
                        focusNode: notesFocusNode,
                        decoration: new InputDecoration(
                          contentPadding: EdgeInsets.only(left: 10.0),
                          hintText: 'add note',
                        ),
                      ),
                      SizedBox(
                        height: 2.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          FlatButton(
                            onPressed: () {
                              notesController.clear();
                              showAddNoteWidget();
                            },
                            child: Text(
                              "CANCEL",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          Row(
                            children: [
                              Text("note at $notesTime"),
                              SizedBox(
                                width: 6,
                              ),
                              RaisedButton(
                                color: Colors.blue,
                                shape: StadiumBorder(),
                                onPressed: () {
                                  var position = flickManager.flickVideoManager
                                      .videoPlayerController.value.position;

                                  String positionInSeconds = position != null
                                      ? (position -
                                              Duration(
                                                  minutes: position.inMinutes))
                                          .inSeconds
                                          .toString()
                                          .padLeft(2, '0')
                                      : null;

                                  var noteTime = position != null
                                      ? '${position.inMinutes}:$positionInSeconds'
                                      : '0:00';

                                  notesArray.add(notesController.text);
                                  notesTimeArray.add(noteTime.toString());

                                  notesController.clear();
                                  flickManager.flickControlManager.play();
                                  showAddNoteWidget();
                                },
                                child: Text("Save"),
                              )
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                ),
              if (isVideoComplets)
                getStartQuizWidget(context, widget.id, widget.title),
              if (isDisplayNote && displayNoteText != "")
                GestureDetector(
                  onTap: () {
                    flickManager.flickControlManager.play();
                    isDisplayNote = false;
                    setState(() {});
                  },
                  child: Container(
                    margin: const EdgeInsets.all(10),
                    height: 50,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(8.0)),
                    child: Center(
                      child: Text(
                        displayNoteText,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }

  showMyNotes() {
    showNotesBottomModalSheet();
  }

  showNotesBottomModalSheet() {
    showModalBottomSheet(
      isDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () {
            Navigator.of(context).pop();
          },
          child: Container(
            color: Color(0xFF737373),
            child: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return LimitedBox(
                  child: Container(
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(20.0),
                            topRight: const Radius.circular(20.0))),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 20.0, left: 30, bottom: 20),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 7,
                                child: Text(
                                  "My Notes",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: IconButton(
                                  alignment: Alignment.center,
                                  icon: Icon(
                                    Icons.close,
                                    color: Colors.black,
                                    size: 30,
                                    semanticLabel: "Close",
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: notesTimeArray.length > 0
                              ? ListView.builder(
                                  itemCount: notesTimeArray.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10.0, right: 10.0),
                                          child: Row(
                                            children: [
                                              Text(
                                                "${index + 1}.  ${notesArray[index].toString()} (at time:${notesTimeArray[index].toString()} s)",
                                                style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Divider()
                                      ],
                                    );
                                  },
                                )
                              : Center(
                                  child: Text(
                                    'No Notes Found',
                                    style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }
}
