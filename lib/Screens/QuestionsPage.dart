import 'dart:async';

import 'package:flutter/material.dart';
import 'package:quiz_app/DataModel/QuestionAnswersModel.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:quiz_app/Resources/RouteConst.dart';

class QuestionPage extends StatefulWidget {
  final int id;
  final String title;

  const QuestionPage({Key key, this.id, this.title}) : super(key: key);
  @override
  _QuestionPageState createState() => _QuestionPageState();
}

class _QuestionPageState extends State<QuestionPage> {
  // QuizDataModel quizData;

  List<Map<String, dynamic>> ansDeatails = [];
  int totalScrore = 0;

  List<QuestionAnswers> questionAnswers = [
    QuestionAnswers(
      question: [
        Questions(
          questionTitle:
              "Brass gets discoloured in air because of the presence of which of the following gases in air?",
          answers: [
            Answers(options: "Oxygen", isRightAns: false, ansPoints: 0),
            Answers(
                options: "Hydrogen sulphide", isRightAns: true, ansPoints: 1),
            Answers(options: "Carbon dioxide", isRightAns: false, ansPoints: 0),
            Answers(options: "Nitrogen", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle:
              "Which of the following is a non metal that remains liquid at room temperature?",
          answers: [
            Answers(options: "Phosphorous", isRightAns: false, ansPoints: 0),
            Answers(options: "Bromine", isRightAns: true, ansPoints: 1),
            Answers(options: "Chlorine", isRightAns: false, ansPoints: 0),
            Answers(options: "Helium", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle:
              "Chlorophyll is a naturally occurring chelate compound in which central metal is",
          answers: [
            Answers(options: "copper", isRightAns: false, ansPoints: 0),
            Answers(options: "iron", isRightAns: false, ansPoints: 0),
            Answers(options: "magnesium", isRightAns: true, ansPoints: 1),
            Answers(options: "calcium", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "Which of the following is used in pencils?",
          answers: [
            Answers(options: "Graphite", isRightAns: true, ansPoints: 1),
            Answers(options: "Silicon", isRightAns: false, ansPoints: 0),
            Answers(options: "Charcoal", isRightAns: false, ansPoints: 0),
            Answers(options: "Phosphorous", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle:
              "Which of the following metals forms an amalgam with other metals?",
          answers: [
            Answers(options: "Tin", isRightAns: false, ansPoints: 0),
            Answers(options: "Mercury", isRightAns: true, ansPoints: 1),
            Answers(options: "Lead", isRightAns: false, ansPoints: 0),
            Answers(options: "Zinc", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "The gas usually filled in the electric bulb is",
          answers: [
            Answers(options: "nitrogen", isRightAns: false, ansPoints: 0),
            Answers(options: "hydrogen", isRightAns: true, ansPoints: 1),
            Answers(options: "carbon dioxide", isRightAns: false, ansPoints: 0),
            Answers(options: "oxygen", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "Washing soda is the common name for",
          answers: [
            Answers(
                options: "Calcium bicarbonate",
                isRightAns: false,
                ansPoints: 0),
            Answers(
                options: "Calcium carbonate", isRightAns: false, ansPoints: 0),
            Answers(
                options: "Sodium bicarbonate", isRightAns: false, ansPoints: 0),
            Answers(options: "Sodium carbonate", isRightAns: true, ansPoints: 1)
          ],
        ),
        Questions(
          questionTitle: "Which of the gas is not known as green house gas?",
          answers: [
            Answers(options: "Carbon dioxide", isRightAns: false, ansPoints: 0),
            Answers(options: "Hydrogen", isRightAns: true, ansPoints: 1),
            Answers(options: "Nitrous oxide", isRightAns: false, ansPoints: 0),
            Answers(options: "Methane", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "Bromine is a",
          answers: [
            Answers(options: "black solid", isRightAns: false, ansPoints: 0),
            Answers(options: "red liquid", isRightAns: true, ansPoints: 1),
            Answers(options: "colourless gas", isRightAns: false, ansPoints: 0),
            Answers(
                options: "highly inflammable gas",
                isRightAns: false,
                ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "The hardest substance available on earth is",
          answers: [
            Answers(options: "Iron", isRightAns: false, ansPoints: 0),
            Answers(options: "Diamond", isRightAns: true, ansPoints: 1),
            Answers(options: "Gold", isRightAns: false, ansPoints: 0),
            Answers(options: "Platinum", isRightAns: false, ansPoints: 0)
          ],
        ),
      ],
    ),
    QuestionAnswers(
      question: [
        Questions(
          questionTitle:
              "In which decade was the American Institute of Electrical Engineers (AIEE) founded?",
          answers: [
            Answers(options: "1880s", isRightAns: true, ansPoints: 1),
            Answers(options: "1850s", isRightAns: false, ansPoints: 0),
            Answers(options: "1930s", isRightAns: false, ansPoints: 0),
            Answers(options: "1950s", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle:
              "What is part of a database that holds only one type of information?",
          answers: [
            Answers(options: "Field", isRightAns: true, ansPoints: 1),
            Answers(options: "Report", isRightAns: false, ansPoints: 0),
            Answers(options: "Record", isRightAns: false, ansPoints: 0),
            Answers(options: "File", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "'OS' computer abbreviation usually means ?",
          answers: [
            Answers(
                options: "Operating System", isRightAns: true, ansPoints: 1),
            Answers(
                options: "Order of Significance",
                isRightAns: false,
                ansPoints: 0),
            Answers(options: "Open Software", isRightAns: false, ansPoints: 0),
            Answers(options: "Optical Sensor", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle:
              "In which decade with the first transatlantic radio broadcast occur?",
          answers: [
            Answers(options: "1900s", isRightAns: true, ansPoints: 1),
            Answers(options: "1870s", isRightAns: false, ansPoints: 0),
            Answers(options: "1860s", isRightAns: false, ansPoints: 0),
            Answers(options: "1860s", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle:
              "'.MOV' extension refers usually to what kind of file?",
          answers: [
            Answers(
                options: "Animation/movie file",
                isRightAns: true,
                ansPoints: 1),
            Answers(options: "Image file", isRightAns: false, ansPoints: 0),
            Answers(options: "Audio file", isRightAns: false, ansPoints: 0),
            Answers(
                options: "MS Office document", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "In which decade was the SPICE simulator introduced?",
          answers: [
            Answers(options: "1950s", isRightAns: false, ansPoints: 0),
            Answers(options: "1960s", isRightAns: false, ansPoints: 0),
            Answers(options: "1970s", isRightAns: true, ansPoints: 1),
            Answers(options: "1980s", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle:
              "Which is a type of Electrically-Erasable Programmable Read-Only Memory?",
          answers: [
            Answers(options: "Flash", isRightAns: true, ansPoints: 1),
            Answers(options: "Flange", isRightAns: false, ansPoints: 0),
            Answers(options: "Fury", isRightAns: false, ansPoints: 0),
            Answers(options: "FRAM", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "The purpose of choke in tube light is ?",
          answers: [
            Answers(
                options: "To decrease the current",
                isRightAns: false,
                ansPoints: 0),
            Answers(
                options: "To increase the current",
                isRightAns: false,
                ansPoints: 0),
            Answers(
                options: "To increase the voltage momentarily",
                isRightAns: true,
                ansPoints: 1),
            Answers(
                options: "MS Office document", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: ".MPG' extension refers usually to what kind of file?",
          answers: [
            Answers(
                options: "WordPerfect Document file",
                isRightAns: false,
                ansPoints: 0),
            Answers(
                options: "MS Office document", isRightAns: false, ansPoints: 0),
            Answers(
                options: "Animation/movie file",
                isRightAns: true,
                ansPoints: 1),
            Answers(options: "Image file", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "Who developed Yahoo?",
          answers: [
            Answers(
                options: "David Filo & Jerry Yang",
                isRightAns: true,
                ansPoints: 1),
            Answers(
                options: "David Filo & Jerry Yang",
                isRightAns: false,
                ansPoints: 0),
            Answers(
                options: "Vint Cerf & Robert Kahn",
                isRightAns: false,
                ansPoints: 0),
            Answers(
                options: "Steve Case & Jeff Bezos",
                isRightAns: false,
                ansPoints: 0)
          ],
        ),
      ],
    ),
    QuestionAnswers(
      question: [
        Questions(
          questionTitle:
              "Which among the following Kavya of Sanskrit, deal with court intrigues & access to power of Chandragupta Maurya?",
          answers: [
            Answers(options: "Mudrarakshahsa", isRightAns: true, ansPoints: 1),
            Answers(options: "Mrichhakatika", isRightAns: false, ansPoints: 0),
            Answers(options: "Ritusamhara", isRightAns: false, ansPoints: 0),
            Answers(options: "Kumarasambhava", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "Which among the following is not a correct pair?",
          answers: [
            Answers(
                options: "Ellora Caves – Rastrakuta Rulers",
                isRightAns: false,
                ansPoints: 0),
            Answers(
                options: "Mahabalipuram – Pallava Rulers",
                isRightAns: false,
                ansPoints: 0),
            Answers(
                options: "Khajuraho – Chandellas",
                isRightAns: false,
                ansPoints: 0),
            Answers(
                options: "Elephanta Caves – Mauyra Era",
                isRightAns: true,
                ansPoints: 1)
          ],
        ),
        Questions(
          questionTitle:
              " On which of the following systems of Hindu Philosophy , Shankaracharya wrote commentary in 9th century AD?",
          answers: [
            Answers(options: "Uttarmimansa", isRightAns: true, ansPoints: 1),
            Answers(options: "Yoga", isRightAns: false, ansPoints: 0),
            Answers(options: "Vaisheshika", isRightAns: false, ansPoints: 0),
            Answers(options: "Sankhya", isRightAns: false, ansPoints: 0)
          ],
        ),
      ],
    ),
    QuestionAnswers(
      question: [
        Questions(
          questionTitle:
              "For some integer n, the odd integer is represented in the form of:",
          answers: [
            Answers(options: "2n+1", isRightAns: true, ansPoints: 1),
            Answers(options: "n", isRightAns: false, ansPoints: 0),
            Answers(options: "n+1", isRightAns: false, ansPoints: 0),
            Answers(options: "2n", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle: "HCF of 26 and 91 is:",
          answers: [
            Answers(options: "13", isRightAns: true, ansPoints: 1),
            Answers(options: "15", isRightAns: false, ansPoints: 0),
            Answers(options: "1", isRightAns: false, ansPoints: 0),
            Answers(options: "2", isRightAns: false, ansPoints: 0)
          ],
        ),
        Questions(
          questionTitle:
              "The addition of a rational number and an irrational number is equal to:",
          answers: [
            Answers(
                options: "rational number", isRightAns: false, ansPoints: 0),
            Answers(
                options: "Irrational number", isRightAns: true, ansPoints: 1),
            Answers(options: "Both", isRightAns: false, ansPoints: 0),
            Answers(
                options: "None of the above", isRightAns: false, ansPoints: 0)
          ],
        ),
      ],
    )
  ];

  int qnsIndex = 0;

  int timer = 30;
  String showtimer = "30";
  bool canceltimer = false;

  @override
  void initState() {
    super.initState();
    starttimer();
  }

  void nextQuestion() {
    setState(() {
      canceltimer = true;

      if (qnsIndex == questionAnswers[widget.id].question.length - 1) {
        Map<String, dynamic> data = {
          "ansDeatails": ansDeatails,
          "totalScore": totalScrore
        };
        Navigator.of(context).pushReplacementNamed(RouteConst.routeCongratsPage,
            arguments: {"data": data});
      } else {
        showtimer = "30";
        timer = 30;
        qnsIndex = qnsIndex + 1;
        canceltimer = false;
        starttimer();
      }
    });
  }

  void starttimer() async {
    const onesec = Duration(seconds: 1);
    Timer.periodic(onesec, (Timer t) {
      setState(() {
        if (timer < 1) {
          t.cancel();
          canceltimer = true;
          AwesomeDialog(
            context: context,
            animType: AnimType.SCALE,
            dialogType: DialogType.INFO,
            dismissOnBackKeyPress: false,
            dismissOnTouchOutside: false,
            body: Center(
              child: Text(
                "Time out",
                style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontSize: 22,
                    fontWeight: FontWeight.bold),
              ),
            ),
            btnOkColor: Colors.blue,
            btnOkOnPress: () {
              ansDeatails.add({"ans_points": 0, "time_taken": 30});
              nextQuestion();
            },
          )..show();
        } else if (canceltimer == true) {
          t.cancel();
        } else {
          timer = timer - 1;
        }
        showtimer = timer.toString();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            Navigator.of(context).pushReplacementNamed(
              RouteConst.routeHomePage,
            );
          },
          child: Icon(Icons.arrow_back_ios, size: 25, color: Colors.white),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            showtimer,
            style: TextStyle(
              fontSize: 35.0,
              fontWeight: FontWeight.w700,
              fontFamily: 'Times New Roman',
            ),
          ),
          Container(
            padding: EdgeInsets.all(16),
            alignment: Alignment.centerLeft,
            child: Text(
              "${qnsIndex + 1}.  ${questionAnswers[widget.id].question[qnsIndex].questionTitle}",
              style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0),
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: questionAnswers[widget.id]
                  .question[qnsIndex]
                  .answers
                  .map((opt) {
                return Container(
                  margin: EdgeInsets.only(bottom: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.blue, width: 1.0)),
                  child: InkWell(
                    onTap: () {
                      // nextQuestion();
                      canceltimer = true;
                      AwesomeDialog(
                        dismissOnBackKeyPress: false,
                        dismissOnTouchOutside: false,
                        context: context,
                        animType: AnimType.SCALE,
                        dialogType: opt.isRightAns
                            ? DialogType.SUCCES
                            : DialogType.ERROR,
                        body: Center(
                          child: Text(
                            opt.isRightAns ? 'Correct Answer' : "Wrong Answer",
                            style: TextStyle(
                                fontStyle: FontStyle.italic,
                                fontSize: 22,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        btnOkColor: opt.isRightAns ? Colors.green : Colors.red,
                        btnOkOnPress: () {
                          ansDeatails.add({
                            "ans_points": opt.isRightAns ? 1 : 0,
                            "time_taken": 30 - int.parse(showtimer)
                          });
                          totalScrore = totalScrore + (opt.isRightAns ? 10 : 0);
                          nextQuestion();
                        },
                      )..show();

                      // _bottomSheet(context, opt.isRightAns);
                    },
                    child: Container(
                      padding: EdgeInsets.all(16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            child: Text(
                              opt.options,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.0),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }
}
