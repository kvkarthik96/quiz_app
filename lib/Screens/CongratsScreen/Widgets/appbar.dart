import 'package:flutter/material.dart';
import 'package:quiz_app/Resources/RouteConst.dart';

Widget appbar(context) {
  return AppBar(
    brightness: Brightness.light,
    elevation: 0.0,
    title: Text(
      "Quiz Result",
      style: TextStyle(color: Colors.white),
    ),
    centerTitle: true,
    leading: InkWell(
      onTap: () {
        Navigator.of(context).pushReplacementNamed(
          RouteConst.routeHomePage,
        );
      },
      child: Icon(Icons.arrow_back_ios, size: 25, color: Colors.white),
    ),
  );
}
