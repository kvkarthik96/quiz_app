import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:quiz_app/Resources/RouteConst.dart';

import 'Widgets/appbar.dart';

class CongratsScreen extends StatefulWidget {
  final List<Map<String, dynamic>> ansDeatails;
  final int totalScore;

  const CongratsScreen({Key key, this.ansDeatails, this.totalScore})
      : super(key: key);
  @override
  _CongratsScreenState createState() => _CongratsScreenState();
}

class _CongratsScreenState extends State<CongratsScreen> {
  @override
  void initState() {
    print(widget.ansDeatails);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pushReplacementNamed(
          RouteConst.routeHomePage,
        );
      },
      child: Scaffold(
        appBar: appbar(context),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.all(15.0),
              width: MediaQuery.of(context).size.width,
              child: Text(
                "Quiz Summary",
                style: TextStyle(
                    color: Colors.green,
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              padding: const EdgeInsets.all(15.0),
              width: MediaQuery.of(context).size.width,
              child: Text(
                "Your Total Scrore ${widget.totalScore} / ${widget.ansDeatails.length * 10} ",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(10.0)),
                child: ListView.builder(
                    itemCount: widget.ansDeatails.length,
                    itemBuilder: (context, index) {
                      bool isRightAns = (widget.ansDeatails[index]['ans_points']
                                  .toString()) ==
                              "1"
                          ? true
                          : false;

                      return Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                                left: 20, right: 20.0, top: 10, bottom: 10),
                            child: Text(
                              "Question No: ${index + 1}, Time Taken: ${widget.ansDeatails[index]['time_taken'].toString()} second(s) is ${isRightAns ? "right answer" : "wrong answer"}  and scored  ${isRightAns ? "10" : "0"} point(s)",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Divider()
                        ],
                      );
                    }),
              ),
            )
          ],
        ),
      ),
    );
  }
}
