import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quiz_app/DataModel/VideoListDataModel.dart';
import 'package:quiz_app/Resources/RouteConst.dart';
import 'package:quiz_app/Utils/HelperUtil.dart';
import 'Widgets/ImageClipper.dart';
import 'Widgets/cardWidget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isNetworkConnected = true;
  var _connectivitySubscription;

  bool isSearchBarEnabled = false;

  List<VideosList> videos = [
    VideosList(
        id: 0,
        title: "General Science",
        subtitle: "First Video",
        thumbnailImage:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLqf3Q0udH7rHX-wzt3FPTFS_9cGJNijBNYQ&usqp=CAU",
        videoUrl:
            "https://media.developer.dolby.com/Atmos/MP4/Universe_Fury2.mp4"),
    VideosList(
        id: 1,
        title: "Technology - Section 1",
        subtitle: "First Video",
        thumbnailImage:
            "https://media.proprofs.com/images/QM/user_images/1132602/1433057376.jpg",
        videoUrl:
            "https://media.developer.dolby.com/DDP/MP4_HPL40_30fps_channel_id_51.mp4"),
    VideosList(
        id: 2,
        title: "History",
        subtitle: "First Video",
        thumbnailImage:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzSuY9cxKKJ-PnpFhvGtjSQO3EpM1Cn0K_zQ&usqp=CAU",
        videoUrl:
            "https://media.developer.dolby.com/DDP/MP4_HPL40_30fps_channel_id_71.mp4"),
    VideosList(
        id: 3,
        title: "Mathematics",
        subtitle: "First Video",
        thumbnailImage:
            "https://miro.medium.com/max/7296/1*deC-mkFzjrzBPL5qEnx-FA.jpeg",
        videoUrl:
            "https://media.developer.dolby.com/Atmos/MP4/shattered-3Mb.mp4"),
  ];

  @override
  void initState() {
    super.initState();

    onNetworkChange();

    HelperUtil.checkInternetConnection().then((internet) {
      if (internet) {
        isNetworkConnected = true;
      } else {
        isNetworkConnected = false;
      }
    });
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  void onNetworkChange() {
    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        if (result == ConnectivityResult.none) {
          isNetworkConnected = false;
        } else {
          isNetworkConnected = true;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0.0,
        title: Text(
          "Quiz App",
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: ListView.builder(
          itemCount: videos.length,
          itemBuilder: (context, index) {
            return cardWidget(index, videos, context);
          }),
    );
  }
}
