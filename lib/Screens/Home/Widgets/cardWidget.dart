import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:quiz_app/Resources/RouteConst.dart';

import 'ImageClipper.dart';

Widget cardWidget(int index, videos, context) {
  return Padding(
    padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
    child: InkWell(
      onTap: () {
        Map<String, dynamic> data = {
          "id": videos[index].id,
          "title": videos[index].title,
          "videoUrl": videos[index].videoUrl,
        };

        Navigator.of(context)
            .pushNamed(RouteConst.routeVideosPage, arguments: {"data": data});
      },
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        color: Colors.transparent,
        elevation: 4.0,
        margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
        child: Stack(
          children: <Widget>[
            new ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    Container(
                      height: 220,
                      child: ConstrainedBox(
                        constraints:
                            BoxConstraints(minHeight: 40, maxHeight: 40),
                        child: ClipPath(
                          clipper: new MyClipper(),
                          child: CachedNetworkImage(
                            width: MediaQuery.of(context).size.width,
                            height: 30,
                            filterQuality: FilterQuality.low,
                            fit: BoxFit.fill,
                            imageUrl: videos[index].thumbnailImage,
                            placeholder: (context, url) => Container(
                              width: MediaQuery.of(context).size.width,
                              height: 30,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        "assets/images/placeholder.jpg"),
                                    fit: BoxFit.cover),
                              ),
                            ),
                            errorWidget: (context, url, error) => Container(
                              width: MediaQuery.of(context).size.width,
                              height: 30,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        "assets/images/placeholder.jpg"),
                                    fit: BoxFit.cover),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      padding: const EdgeInsets.only(
                          top: 15, left: 12.0, right: 12.0, bottom: 12.0),
                      child: Text(
                        videos[index].title,
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              right: 15.0,
              top: 185.0,
              child: Container(
                height: 50.0,
                width: 50.0,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: Icon(
                    Icons.play_arrow,
                    size: 25.0,
                    color: Colors.white,
                    semanticLabel: 'Play',
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    ),
  );
}
