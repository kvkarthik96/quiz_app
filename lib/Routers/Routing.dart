import 'package:flutter/material.dart';
import 'package:quiz_app/Resources/RouteConst.dart';
import 'package:quiz_app/Screens/CongratsScreen/CongratsScreen.dart';
import 'package:quiz_app/Screens/Home/HomePage.dart';
import 'package:quiz_app/Screens/QuestionsPage.dart';
import 'package:quiz_app/Screens/videoScreen/Video.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case RouteConst.routeDefault:
        return MaterialPageRoute(builder: (_) => HomePage());
        break;

      case RouteConst.routeHomePage:
        return MaterialPageRoute(builder: (_) => HomePage());
        break;

      case RouteConst.routeQuestionsPage:
        if (args != "" && args != null) {
          Map<String, dynamic> data;
          data = args;
          return MaterialPageRoute(
            builder: (_) => QuestionPage(
              id: data['data']['id'],
              title: data['data']['title'],
            ),
          );
        }

        break;

      case RouteConst.routeVideosPage:
        if (args != "" && args != null) {
          Map<String, dynamic> data;
          data = args;
          return MaterialPageRoute(
            builder: (_) => VideoScreen(
              id: data['data']['id'],
              title: data['data']['title'],
              videoUrl: data['data']['videoUrl'],
            ),
          );
        }
        break;
      case RouteConst.routeCongratsPage:
        if (args != "" && args != null) {
          Map<String, dynamic> data;
          data = args;
          return MaterialPageRoute(
            builder: (_) => CongratsScreen(
                ansDeatails: data['data']['ansDeatails'],
                totalScore: data['data']['totalScore']),
          );
        }
        return MaterialPageRoute(builder: (_) => CongratsScreen());
        break;

      default:
        return _errorRoute(settings.name);
    }
  }

  static Route<dynamic> _errorRoute(pageName) {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.error_outline),
              Text(
                "Error Routing Page Not Found : " + pageName.toString(),
                style: TextStyle(fontSize: 18.0),
              )
            ],
          ),
        ),
      );
    });
  }
}
