class QuestionAnswersModel {
  List<QuestionAnswers> questionAnswers;

  QuestionAnswersModel({this.questionAnswers});

  QuestionAnswersModel.fromJson(Map<String, dynamic> json) {
    if (json['question_answers'] != null) {
      questionAnswers = new List<QuestionAnswers>();
      json['question_answers'].forEach((v) {
        questionAnswers.add(new QuestionAnswers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.questionAnswers != null) {
      data['question_answers'] =
          this.questionAnswers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class QuestionAnswers {
  List<Questions> question;

  QuestionAnswers({this.question});

  QuestionAnswers.fromJson(Map<String, dynamic> json) {
    if (json['question'] != null) {
      question = new List<Questions>();
      json['question'].forEach((v) {
        question.add(new Questions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.question != null) {
      data['question'] = this.question.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Questions {
  String questionTitle;
  List<Answers> answers;

  Questions({this.questionTitle, this.answers});

  Questions.fromJson(Map<String, dynamic> json) {
    questionTitle = json['question_title'];
    if (json['answers'] != null) {
      answers = new List<Answers>();
      json['answers'].forEach((v) {
        answers.add(new Answers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['question_title'] = this.questionTitle;
    if (this.answers != null) {
      data['answers'] = this.answers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Answers {
  String options;
  bool isRightAns;
  int ansPoints;

  Answers({this.options, this.isRightAns, this.ansPoints});

  Answers.fromJson(Map<String, dynamic> json) {
    options = json['options'];
    isRightAns = json['is_right_ans'];
    ansPoints = json['ans_points'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['options'] = this.options;
    data['is_right_ans'] = this.isRightAns;
    data['ans_points'] = this.ansPoints;
    return data;
  }
}
