class VideoListDataModel {
  List<VideosList> videosList;

  VideoListDataModel({this.videosList});

  VideoListDataModel.fromJson(Map<String, dynamic> json) {
    if (json['videos_list'] != null) {
      videosList = new List<VideosList>();
      json['videos_list'].forEach((v) {
        videosList.add(new VideosList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.videosList != null) {
      data['videos_list'] = this.videosList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class VideosList {
  int id;
  String title;
  String thumbnailImage;
  String videoUrl;
  String subtitle;

  VideosList(
      {this.id, this.title, this.thumbnailImage, this.videoUrl, this.subtitle});

  VideosList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    thumbnailImage = json['thumbnail_image'];
    videoUrl = json['video_url'];
    subtitle = json['subtitle'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['thumbnail_image'] = this.thumbnailImage;
    data['video_url'] = this.videoUrl;
    data['subtitle'] = this.subtitle;
    return data;
  }
}
